import os
import logging
import datetime
import matplotlib.pyplot as plt
import settings
from fpdf import FPDF
from db import get_sum



report_logging = logging.getLogger("Oxobot.report")


def create_pie_chart_pdf(tgm_id, date_from, date_to=None):
    path = settings.PATH+"/content/{0}".format(tgm_id)
    png_file = path+"/{0}_pie_chart_{1}.png".format(tgm_id, datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S"))
    pdf_file = path+"/{0}_report_{1}.pdf".format(tgm_id, datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S"))
    labels = []
    cost_sum = []
    logger = logging.getLogger("Oxobot.report.pdf")
    expences_report = get_sum(tgm_id, date_from, date_to)
    try:
        logging.info(("User %s requests sum of expences for pie chart pdf from %s to %s") % (tgm_id, date_from, date_to))
        sum_all = sum(element[1] for element in expences_report)
    except TypeError as ex:
        logger.exception("Failure %s" % (ex))

    for element in expences_report:
        labels.append(element[0])
        cost_sum.append(element[1])

    fig1, ax1 = plt.subplots()
    ax1.pie(cost_sum, labels=labels, autopct='%1.2f%%', shadow=True, startangle=90)
    ax1.axis('equal')  
    if not os.path.exists(path):
        try:
            logging.info("Create directory for user %s" % (tgm_id))
            os.makedirs(path)
        except FileNotFoundError as ex:
            logger.exception("Failure %s" % (ex))
    plt.savefig(png_file)

    logging.info(("User %s try create pdf in %s") % (tgm_id, pdf_file))
    
    pdf = FPDF()
    pdf.add_page()
    pdf.add_font("DejaVu Serif", fname=os.path.abspath(os.path.dirname(os.path.abspath(__file__))) + "/font/dejavu-serif.ttf", uni=True)
    pdf.set_font("DejaVu Serif", size=16)
    #pdf.ln(85)  # ниже на 85
    pdf.cell(200, 10, txt="Анализ расходов за период с " + str(date_from) + " по " + str(date_to), ln=1,  align="C")
    pdf.image(png_file, x=10, y=100, w=200)
    


    col_width = pdf.w / 4.5
    row_height = pdf.font_size
    for row in expences_report:
        for item in row:
            pdf.cell(col_width, row_height*1, txt=str(item), border=1)
        pdf.ln(row_height*1)

    pdf.output(pdf_file)

    os.remove(png_file)
    return pdf_file

#create_pie_chart_pdf("150186083", "2019-12-11", "2019-12-18")