#Этот Бот умеет записывать, хранить и подсчитывать твои расходы
#Команды:
# /start - начать работу с ботом
# /add_expences - добавить потраченную сумму
# /add_category - добавить категорию расходов
# /get_expences - вывести таблицу с расходами
# /create_report - вывести отчет по расходам
# /delete_expences - удалить запись о затратах
# /help - вывести справку 


import os
import logging
import telebot
import datetime
import settings
from telebot import types
from db import get_expences_db, get_categories, create_category
from db import create_expences, create_user, is_category_exist, is_user_exist
from db import delete_expences_db
from report import create_pie_chart_pdf


bot = telebot.TeleBot(settings.TOKEN)

logger = logging.getLogger("Oxobot")
logger.setLevel(logging.INFO)
fh = logging.FileHandler(settings.PATH+"/logs/server_{}.log".format(datetime.datetime.today().strftime("%m-%d-%Y")))
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)


@bot.message_handler(commands=["add_expences"])
def get_command(message):
    bot.send_message(message.from_user.id, "Добавь сумму расхода")
    bot.register_next_step_handler(message, choose_category)


def choose_category(message):
    keys = types.InlineKeyboardMarkup()
    categories = get_categories(message.from_user.id)
    for element in categories:
        keys.add(types.InlineKeyboardButton(text=element, callback_data=element))
    global cost
    cost = message.text
    bot.send_message(message.from_user.id, "На что потратил?", reply_markup=keys)


@bot.callback_query_handler(func=lambda call:True)
def get_cost(call):
    tgm_id = call.from_user.id
    cat_name = call.data
    create_expences(tgm_id, cat_name, cost)
    bot.answer_callback_query(call.id, "Записал")


@bot.message_handler(commands=["add_category"])
def add_category(message):
    bot.send_message(message.from_user.id, "Введи название для категории")
    bot.register_next_step_handler(message, enter_category)
    

def enter_category(message):
    cat_name = message.text
    tgm_id = message.from_user.id
    if is_category_exist(tgm_id, cat_name):
        bot.send_message(message.from_user.id, "Категория уже существует. Попробуй еще раз")
        bot.register_next_step_handler(message, enter_category)
    else:
        create_category(tgm_id, cat_name)
        bot.send_message(message.from_user.id, "Категория добавлена")


@bot.message_handler(commands=["get_expences"])
def get_expences(message):
    bot.send_message(message.from_user.id, "\
Введи дату начала выборки(невключительно) в формате YYYY-MM-DD.\n\
Можешь дополнительно указать дату окончания выборки через пробел.")
    bot.register_next_step_handler(message, enter_date)


def enter_date(message):
    m_text = message.text
    try:
        dates = m_text.split()
        if len(dates) == 2:
            expences = get_expences_db(
                tgm_id=message.from_user.id, 
                date_from=dates[0], 
                date_to=dates[1])
        elif len(dates) == 1:
            expences = get_expences_db(
                tgm_id=message.from_user.id, 
                date_from=dates[0])
        bot.send_message(message.from_user.id, expences)
    except Exception as ex:
        logger.exception("Failure %s in:" % (ex))


@bot.message_handler(commands=["delete_expences"])
def delete_expences(message):
    bot.send_message(message.from_user.id, "Введи номер записи, чтобы удалить её")
    bot.register_next_step_handler(message, enter_expences)


def enter_expences(message):
    tgm_id = message.from_user.id
    expences_id = message.text
    bot.send_message(message.from_user.id, delete_expences_db(tgm_id, expences_id))


@bot.message_handler(commands=["create_report"])
def create_report(message):
    bot.send_message(message.from_user.id, "\
Введи дату начала выборки(невключительно) в формате YYYY-MM-DD.\n\
Можешь дополнительно указать дату окончания выборки через пробел.")
    bot.register_next_step_handler(message, get_date_for_report)


def get_date_for_report(message):
    m_text = message.text
    try:
        dates = m_text.split()
        if len(dates) == 2:
            path_to_pdf = create_pie_chart_pdf(
                tgm_id=message.from_user.id, 
                date_from=dates[0], 
                date_to=dates[1])
        elif len(dates) == 1:
            path_to_pdf = create_pie_chart_pdf(
                tgm_id=message.from_user.id, 
                date_from=dates[0])
        report = open(path_to_pdf, 'rb')
        bot.send_document(message.from_user.id, report)
    except Exception as ex:
        logger.exception("Failure %s in:" % (ex))


@bot.message_handler(commands=["start"])
def get_text_messages(message):
    tgm_id = message.from_user.id
    username = message.from_user.username
    if is_user_exist(tgm_id):
        bot.send_message(
            message.from_user.id,
            "Привет, "
            + str(username)
            + ", рад видеть тебя снова")
    else:
        create_user(username, tgm_id)
        bot.send_message(
            message.from_user.id,
            "Приятно познакомиться, "
            + str(username)
            + ", нажми /add_category, чтобы создать категорию расходов"
        )


@bot.message_handler(commands=["help"])
def get_help(message):
    username = message.from_user.username
    bot.send_message(
            message.from_user.id,
            str(username)
            + ", Меня зовут YourExpensesBot! Я умею записывать, хранить и подсчитывать твои расходы.\n\
Ты можешь мной управлять с помощью команд:\n\
/start - начать работу с ботом\n\
/add_expences - добавить потраченную сумму\n\
/add_category - добавить категорию расходов\n\
/get_expences - вывести таблицу с расходами\n\
/create_report - вывести отчет по расходам\n\
/delete_expences - удалить запись о затратах\n\
/help - вывести справку")


@bot.message_handler(content_types=["text"])
def dont_understand(message):
    tgm_id = message.from_user.id
    logging.info(("User %s requests /help") % (tgm_id))
    username = message.from_user.username
    bot.send_message(
            message.from_user.id,
            str(username)
            + ", Я тебя не понимаю, нажми /help")


try:
    logger.info("Oxobot is started!")
    bot.polling(none_stop=True, interval=0)
except Exception as ex:
    logger.exception("Failure %s in:" % (ex))
    