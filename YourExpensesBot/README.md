# YourExpensesBot

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

YourExpensesBot is a python-telegram bot for calculate your expenses.
Components:
  - SQLite
  - Telebot lib
  - My sweat and blood

## Features!

You may:
  - Create and manage expenses category
  - Create and manage expenses
  - Create a report in telegram interface
  - And download a report in PDF

> This is a course project work. To create it, I studied and used: decorators, working with a database, working with various modules and libraries, logging all actions. Be sure to follow all the recommendations of PEP.

### Install

Dillinger uses a number of open source projects to work properly:

* [Python](https://www.python.org/) - programming language
* [SQLite](https://www.sqlite.org/index.html) - SQL database engin
* [FPDF](https://pypi.org/project/fpdf/) - A library for PDF document generation under Python
* [Telebot](https://github.com/eternnoir/pyTelegramBotAPI) - A simple, but extensible Python implementation for the Telegram Bot API.
* [Matplotlib.pyplot](https://pypi.org/project/matplotlib/) - A library to produce publication quality 2D graphics
* [Logging](https://pypi.org/project/logging/) - This module is intended to provide a standard error logging mechanism in Python as per PEP 282.


### Installation

* Install some modules for Python.

```sh
$ pip install pyTelegramBotAPI
$ pip install FPDF
$ pip install matplotlib.pyplot
$ pip install logging
```

* Download source code from repositories (requires git):

```sh
$ git clone https://gitlab.com/OksanaSlo/ucheba.git
$ cd YourExpensesBot
```

* Configure `settings.py`:
  * TOKEN = your telegram bot token
  * URL = `if need` edit connection string
 
* Run `api.py`
