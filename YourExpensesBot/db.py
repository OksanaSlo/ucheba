import os
import logging
import datetime
import sqlalchemy as sa
import settings
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


module_logger = logging.getLogger("Oxobot.db")

Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)


class User(Common, Base):
    __tablename__ = "users"

    username = sa.Column(sa.String(256), nullable=False, unique=True)
    tgm_id = sa.Column(sa.Integer, unique=True)


class Category(Common, Base):
    __tablename__ = "categories"

    user_id = sa.Column(sa.Integer, sa.ForeignKey(f"{User.__tablename__}.id"), nullable=False)
    cat_name = sa.Column(sa.String(50), nullable=False)


class Expenses(Common, Base):
    __tablename__ = "expences"
    user_id = sa.Column(sa.Integer, sa.ForeignKey(f"{User.__tablename__}.id"), nullable=False)
    category_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Category.__tablename__}.id"), nullable=False)
    cost = sa.Column(sa.Integer, nullable=False)
    created_at = sa.Column(sa.DateTime, nullable = False, default=datetime.datetime.now)

engine = sa.create_engine(settings.URL)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Base.metadata.create_all(engine)

def is_user_exist(tgm_id):
    try:
        logger = logging.getLogger("Oxobot.db.user")
        for element in session.query(User):
            if element.tgm_id == tgm_id:
                session.close()
                logger.info("User %s exists" % (tgm_id))
                return True
            else:
                session.close()
                logger.info("User %s doesn't exist" % (tgm_id))
                return False
    except Exception as ex:
        logger.exception("Wrong input %s" % (ex))


def create_user(username, tgm_id):
    logger = logging.getLogger("Oxobot.db.create")
    try:
        user = User(username=username, tgm_id=tgm_id)
        session.add(user)
        session.commit()
        logger.info("Create account %s for user " % (tgm_id))
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def is_category_exist(tgm_id, cat_name):
    logger = logging.getLogger("Oxobot.db.select")
    logger.info("User %s try find %s in DB" % (tgm_id, cat_name))
    try:
        result = False
        for element in session.query(Category).join(User, Category.user_id == User.id).filter(User.tgm_id == tgm_id):
            if element.cat_name == cat_name:
                logger.info("Found %s entities with id %s" % (cat_name, element.id))
                result = True
                break
        logger.info("Not found %s category for user %s" % (cat_name, tgm_id))
        session.close()
        return result
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def create_category(tgm_id, cat_name):
    logger = logging.getLogger("Oxobot.db.create")
    try:
        logger.info("User %s try create category with name %s in DB" % (tgm_id, cat_name))
        user_id = session.query(User).filter(User.tgm_id == tgm_id).first()
        category = Category(user_id=user_id.id, cat_name=cat_name)
        session.add(category)
        session.commit()
        logger.info("For user %s created category with name %s" % (tgm_id, cat_name))
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def get_categories(tgm_id):
    logger = logging.getLogger("Oxobot.db.select")
    categories = []
    try:
        logger.info("User %s find categories in DB" % (tgm_id))
        for element in session.query(Category).join(User, Category.user_id == User.id).filter(User.tgm_id == tgm_id):
            categories.append(element.cat_name)
        session.close()
        logger.info("User %s gets categories: %s" % (tgm_id, categories))
        return categories
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def create_expences(tgm_id, cat_name, cost):
    logger = logging.getLogger("Oxobot.db.create")
    try:
        logger.info("User %s try add new expences in DB" % (tgm_id))
        user_id = session.query(User).filter(User.tgm_id == tgm_id).first()
        category_id = session.query(Category).filter(Category.cat_name == cat_name).first()
        expenses = Expenses(user_id=user_id.id, category_id=category_id.id, cost=cost)
        session.add(expenses)
        session.commit()
        logger.info("User %s spend %s money for %s category" % (tgm_id, cost, cat_name))
    except Exception as ex:
        logger.exception("Failure %s" % (ex))

def get_expences_db(tgm_id, date_from=datetime.date.today(), date_to=None):
    logger = logging.getLogger("Oxobot.db.select")
    try:
        result = ""
        result += "id - Категория - Сумма\n"
        logger.info("User %s find expences in DB with date from %s to %s" % (tgm_id, date_from, date_to))
        user_id = session.query(User).filter(User.tgm_id == tgm_id).first()
        query = session.query(Expenses, Category).join(Category, Category.id == Expenses.category_id)
        expenses = query.filter(Expenses.user_id == user_id.id, Expenses.created_at >= date_from)
        if date_to:
            expenses = expenses.filter(Expenses.created_at <= date_to)
        for expens, category in expenses:
            result += ("%s - %s - %s\n" % (expens.id, category.cat_name, expens.cost))
        
        logger.info("Get list with %s elements" % (len(result)))
        session.close()
        return result
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def delete_expences_db(tgm_id, expenses_id):
    logger = logging.getLogger("Oxobot.db.delete")
    try:
        logger.info("User %s try delete expences in DB with id %s" % (tgm_id, expenses_id))
        user_id = session.query(User).filter(User.tgm_id == tgm_id).first()
        query = session.query(Expenses).filter(Expenses.user_id == user_id.id, Expenses.id == expenses_id)
        if query.first():
            logger.info("Expences %s was found on the %s" % (expenses_id, tgm_id))
            query.delete()
            session.commit()
            return "Запись удалена"
        else:
            session.close()
            logger.info("Expences %s was not found on the %s" % (expenses_id, tgm_id))
            return "Запись не найдена у пользователя"
    except Exception as ex:
        logger.exception("Failure %s" % (ex))


def get_sum(tgm_id, date_from=datetime.date.today(), date_to=None):
    logger = logging.getLogger("Oxobot.db.select")
    try:
        logger.info("User %s requests expences report grouped by category from %s to %s" % (tgm_id, date_from, date_to))
        user_id = session.query(User).filter(User.tgm_id == tgm_id).first()
        cat_sum = session.query(Category.cat_name, func.sum(Expenses.cost)).join(Category, Category.id == Expenses.category_id)
        query = cat_sum.filter(Expenses.user_id == user_id.id, Expenses.created_at >= date_from).group_by(Category.cat_name)
        if date_to:
            query = query.filter(Expenses.created_at <= date_to)
        session.close()
        return query.all()

    except Exception as ex:
        logger.exception("Failure %s" % (ex))









