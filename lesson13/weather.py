import requests

city_name = input("Enter city name: ")

response = requests.get(
    "http://api.openweathermap.org/data/2.5/weather",
    params={
        "q": city_name,
        "units": "metric",
        "appid": "5d1c5370369029f2d3d9274729db73b2",
    }
)
json = response.json()
weather_main = json['weather'][0]['main']
weather_desc = json['weather'][0]['description']
print("Weather in", json['name'])
print("{}, {}".format(weather_main,weather_desc))
print("Temperature:", json['main']['temp'], "С°")
print("Max temperature:", json['main']['temp_max'], "С°")
print("Min temperature:", json['main']['temp_min'], "С°")