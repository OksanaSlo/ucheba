import requests

def get_json(city_name):
 response = requests.get(
 "http://api.openweathermap.org/data/2.5/weather",
 params={
  "q": city_name,
  "units": "metric",
  "appid": "5d1c5370369029f2d3d9274729db73b2",
  }
 )
 json = response.json()
 return json

def desc(json):
 intro = "Weather in " + str(json['name']) + ": "
 weather_main = json['weather'][0]['main']
 weather_desc = json['weather'][0]['description']
 rain = "Rain"
 if rain in weather_desc or rain in weather_main:
  add = "It's raining. Don't forget an umbrella!"
  return intro, add + "\n"
 return intro, weather_main + "\n"


def get_temp(json):
 temp = json['main']['temp']
 if temp >= 30:
  wear = "Temperature: " +  str(temp) + " С°. It's too hot! Put on a swim suit!"
 elif temp <= 29 and temp > 20:
  wear = "Temperature: " +  str(temp) + " С°. It's hot! Put on a shirt and shorts!"
 elif temp <= 20 and temp > 10:
  wear = "Temperature: " +  str(temp) + " С°. Put on a leather jacket!"
 elif temp <=10 and temp > 0:
  wear = "Temperature: " +  str(temp) + " С°. Put on a coat!"
 elif temp <= 0 and temp > -15:
  wear = "Temperature: " +  str(temp) + " С°. Put on a down jacket!"
 elif temp >= -15 and temp < -30:
  wear = "Temperature: " +  str(temp) + " С°. It's freezing! Put on a fur jacket!"
 else:
  wear = "Temperature: " +  str(temp) + " С°. It's strong cold! Stay home!"
 
 return wear + "\n"


def wind_direction(json):
 try:
  deg = json['wind']['deg']
  if deg > 330 or deg <= 30:
   direction = "Wind direction is nord."
  elif deg > 30 and deg <= 60:
   direction = "Wind direction is nord-east."
  elif deg > 60 and deg <= 120:
   direction = "Wind direction is east."
  elif deg > 120 and deg <= 150:
   direction = "Wind direction is south-east."
  elif deg > 150 and deg <= 210:
   direction = "Wind direction is south."
  elif deg > 210 and deg <= 240:
   direction = "Wind direction is south-west."
  elif deg > 240 and deg <= 300:
   direction = "Wind direction is west."
  else:
   direction = "Wind direction is nord-west."
  return direction + "\n"
  
 except KeyError:
  return "The weather is calm\n"