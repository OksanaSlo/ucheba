import logging
module_logger = logging.getLogger("exampleApp.ops")

def add(x, y):
 logger = logging.getLogger("exampleApp.ops.add")
 logger.info("Result is %s" % (x + y))
 return x + y


def sub(x, y):
 logger = logging.getLogger("exampleApp.ops.add")
 logger.info("Result is %s" % (x - y))
 return x - y


def multi(x, y):
 logger = logging.getLogger("exampleApp.ops.add")
 logger.info("Result is %s" % (x * y))
 return x * y


def div(x, y):
 logger = logging.getLogger("exampleApp.ops.add")
 try:
  z = x / y
  logger.info("Result is %s" % (z))
  return z
 except ZeroDivisionError as exception:
  logger = logging.getLogger("exampleApp.ops.add")
  logger.error(exception)
  logger.error("User tried to divide %s to 0" % (x))

def exp(x, y):
 logger = logging.getLogger("exampleApp.ops.add")
 logger.info("Result is %s" % (x ** y))
 return x ** y