import logging
import os
from calc import ops


def main():
    logger = logging.getLogger("exampleApp")
    logger.setLevel(logging.INFO)
    path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
    fh = logging.FileHandler(path+r"\history.log")
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logger.info("Program started!")

    def firstNum():
        try:
            x = float(input("Enter the first number:"))
            logger.info("User entered the first number %s" % (x))
            return float(x)
        except ValueError as ex:
            logger.error("Wrong input %s" % (ex))
            firstNum()

    def secondNum():
        try:
            y = float(input("Enter the second number:"))
            logger.info("User entered the second number %s" % (y))
            return float(y)
        except ValueError as ex:
            logger.error("Wrong input %s" % (ex))
            secondNum()
    x = firstNum()
    y = secondNum()
    d = input("What do you want to do?")

    if d == "+":
        logger.info("User wants to add %s and %s" % (x, y))
        from calc.ops import add
        print("Addition is", add(x, y))

    elif d == "-":
        logger.info("User wants to substract %s and %s" % (x, y))
        from calc.ops import sub
        print("Subtraction is", sub(x, y))

    elif d == "*":
        logger.info("User wants to multiply numbers %s and %s" % (x, y))
        from calc.ops import multi
        print("Multiplication is", multi(x, y))

    elif d == "/":
        logger.info("User wants to divide numbers %s and %s" % (x, y))
        from calc.ops import div
        print("Division is", div(x, y))

    elif d == "**":
        logger.info("User wants to raise %s to the %s digree" % (x, y))
        from calc.ops import exp
        print("Exponentiation is", exp(x, y))

    else:
        print("Error")
        logger.error("Wrong input %s" % (d))


if __name__ == "__main__":
    main()
