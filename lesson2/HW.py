day = int(input())
month = int(input())
year = int(input())
today = "4.10.2019"
dayarray = (day >= 1 and day <= 30 and month in [4, 6, 9, 11]) or (day >= 1 and day <= 31 and month in [1, 3, 5, 7, 8, 10, 12]) or (day in [28, 29] and month == 2)
if dayarray:
	if year >= 2019 and (month > 10 or (month == 10 and day > 4)):
		print("You are from future!")
	elif month < 10 or (month == 10 and day <= 4):
		print("You are " +str(2019 - year)+ " years old!")
	else:
		print("You are " +str(2018 - year)+ " years old!")
else:
	print("Wrong date!")