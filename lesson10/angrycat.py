import random

score = 0
turn = 1
playerTurn = True
currentuser = {playerTurn : "Игрок", not playerTurn : "Компьютер"}
print("Игра \"Доведи кота\"")
print("Уровень стресса кота: " + str(score))
print("Вводите числа насколько хотите разозлить кота от 1 до 10")

def angry():
    if turn % 2 != 0:
        x = int(input("Введите число: "))
        if 1 <= x <= 10:
            print("Вы позлили кота на %s" % x)
            return int(x)
        else:
            print("Не злите его слишком сильно!")
            return angry()
    elif turn % 2 == 0:
        y = random.randint(1, 10)
        print("Компьютер позлил кота на %s" % y)
        return int(y)

print("Первый ход. Начинает игрок")

while score < 100:
    score = score + angry()
    playerTurn = not playerTurn
    turn = turn + 1
    print("Ход: %s Ходит %s. Кот зол на %s из 100" % (turn, currentuser[playerTurn] ,score))


print("Тикай с городу. Кот в ярости. Победил, к его сожалению: " + currentuser[not playerTurn])