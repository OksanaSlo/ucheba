a = int(input())
vis = (((a % 4) == 0 and (a%100) != 0) or a%400 == 0) and a <= 30000

if vis:
	print("YES")
else:
	print("NO")