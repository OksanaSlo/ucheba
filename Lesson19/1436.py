x = int(input())
N = list(map(int, input().split()))

for index in range(1, x):
    while index > 0 and N[index] < N[index - 1]:
        N[index], N[index - 1] = N[index - 1], N[index]
        print(*N)
        index -= 1