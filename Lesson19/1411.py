N = int(input())
n = list(map(int, input().split()))
it = 0

for i1 in range(1, N + 1):
    for i2 in range(0, N - i1):
        if n[i2] > n[i2 + 1]:
            n[i2], n[i2 + 1] = n[i2 + 1], n[i2]
            it += 1
            
print(it)