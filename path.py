import os
x = os.path.dirname(os.path.abspath(__file__))
print(x)

path_to_current_file = os.path.realpath(__file__)
print('path to the current file:', path_to_current_file)
 
path_to_current_folder = os.path.dirname(path_to_current_file)
print('path to the current folder:', path_to_current_folder)
 
folder_inside_current_folder = os.path.dirname(os.path.join(path_to_current_folder, 'static/'))
print('path to the custom folder:', folder_inside_current_folder)

dir_path = os.path.dirname(os.path.abspath(os.path.join(os.path.realpath(__file__), os.pardir)))
print(dir_path)

h = os.path.join(os.path.dirname(__file__),'..','b','history.log')
print(h)