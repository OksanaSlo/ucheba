import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)


class Client(Common, Base):
    __tablename__ = "clients"

    org = sa.Column(sa.String(256), nullable=False)
    phone = sa.Column(sa.String(12), nullable=False)


class Order(Common, Base):
    __tablename__ = "orders"

    client_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Client.__tablename__}.id"), nullable=False)
    client = relationship(Client, uselist=True)
    device = sa.Column(sa.String(256), nullable=False)
    count = sa.Column(sa.Integer, default=1, nullable=False)
    price = sa.Column(sa.Integer, nullable=False)


class Delivery(Common, Base):
    __tablename__ = "delivery"

    order_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Order.__tablename__}.id"), nullable=False)
    adress = sa.Column(sa.String(256), nullable=False)
    delivery_date = sa.Column(sa.Date, nullable=False, default=datetime.date.today)


class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite:///database3.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()
        
        Base.metadata.create_all(self.engine)

    def get_schedule(self):
        return self.session.query(Order).all()


class ClientRole(Role):
    def register(self, org, phone):
        client = Client(org=org, phone=phone)
        self.session.add(client)
        self.session.commit()
        self.record = client

    def make_order(self, org, device, count, price):
        client_id = self.session.query(Client).filter(Client.org == org).first()
        order = Order(client_id=client_id.id, device=device, count=count, price=price)
        self.session.add(order)
        self.session.commit()
        self.record = order

        
class DeliveryRole(Role):
    def deliver(self, order, adress, delivery_date):
        order_id = self.session.query(Order).filter(Order.id == order).first()
        delivery = Delivery(order_id=order_id.id, adress=adress, delivery_date=delivery_date)
        self.session.add(delivery)
        self.session.commit()
        self.record = delivery


client = ClientRole()
client.register(org = "ZAO RGB", phone = "899999999")
client.make_order(client_id = 2, device = "cofeemachine", count = 4, price = "34000")
client.make_order(org = "ZAO RGB", device = "boiler", count = 1, price = "15000")
delivery = DeliveryRole()
delivery.deliver(order_id = 1, adress = "SPB, Sadovaya, 41", delivery_date = datetime.datetime.strptime('20191212', '%Y%m%d').date())
delivery.deliver(2, "Lesnaya, 11", delivery_date = datetime.datetime.strptime('20191214', '%Y%m%d').date())

