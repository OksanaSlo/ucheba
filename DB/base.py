import datetime
import pymssql
import sqlalchemy as sa 
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


Base = declarative_base()


class Common:
    id = sa.Column(sa.Integer, primary_key = True)
    created_at = sa.Column(sa.DateTime, default=datetime.datetime.now, nullable = False)


class Product(Common, Base):
    __tablename__ = "products"

    name = sa.Column(sa.String(64), unique=True, nullable=False)
    price = sa.Column(sa.Float, default=0)


class Statistics(Common, Base):
    __tablename__ = "statistics"

    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    product = relationship(Product, uselist=True)
    count = sa.Column(sa.Integer, default=0, nullable=False)


URL = 'mssql+pymssql://sa:123456@LAPTOP-QNQ0AFPS/MyDataBase'#?driver=SQL+Server+Native+Client+17.0'
engine = sa.create_engine(URL)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Base.metadata.create_all(engine)

p1 = Product(name="Apple", price=80.5)
p2 = Product(name="Limon", price=125.9)
p3 = Product(name="Abricot", price=346.0)

session.add_all([p1, p2, p3])

app1 = Statistics(product_id=1, count=345)
app2 = Statistics(product_id=2, count=78)
app3 = Statistics(product_id=3, count=677)

session.add_all([app1, app2, app3])
session.commit()

session.close()